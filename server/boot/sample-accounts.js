module.exports = function(app) {
  var Account = app.models.Account;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping; 
  Account.find({where:{username:'admin2'}},function(err,usr){
    console.log("err",err);
    console.log("usr",usr);
    if(!usr || (Array.isArray(usr) && usr.length<=0)){
      Account.create([
        {name:'erick',username: 'admin2', email: 'ecavaloss2@gmail.com', password: 'admin123'}, 
      ], function(err, users) {
        if (err) throw err;

        console.log('Created users:', users);

        Role.find({where:{name:'admin'}},function(err,rol){
          if(!rol || (Array.isArray(rol) && rol.length<=0)){ 
            //create the admin role
            Role.create({
              name: 'admin'
            }, function(err, role) {
              if (err) throw err;

              console.log('Created role:', role);

              //make bob an admin
              role.principals.create({
                principalType: RoleMapping.USER,
                principalId: users[0].id
              }, function(err, principal) {
                if (err) throw err;

                console.log('Created principal:', principal);
              });
            });
          }
        });
      });
    }
  }); 
};